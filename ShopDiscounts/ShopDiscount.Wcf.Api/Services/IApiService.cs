﻿using ShopDiscount.Wcf.Api.PersisterServiceReference;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace ShopDiscount.Wcf.Api.Services
{
    [ServiceContract]
    public interface IApiService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/test", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        string Test();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/details?articleId={articleId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Article GetArticleDetals(long articleId);

        //[OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "/list?count={count}&page={page}&order={order}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        //SimpleArticle[] GetArticleList(int count, int page, ArticleOrder order);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/list?count={count}&page={page}&order={order}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        ArticleListModel GetArticleListModel(int count, int page, ArticleOrder order);
    }
}
