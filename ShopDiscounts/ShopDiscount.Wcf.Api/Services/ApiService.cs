﻿using System.Collections.Generic;
using System.ServiceModel.Activation;
using ShopDiscount.Wcf.Api.PersisterServiceReference;
using System;
using log4net;
using System.Reflection;

namespace ShopDiscount.Wcf.Api.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ApiService : IApiService
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public Article GetArticleDetals(long articleId)
        {
            try
            {
                _log.InfoFormat("GetArticleDetals : articleId={0}", articleId);
                using (var proxy = new PersisterServiceClient())
                {
                    var article = proxy.GetArticleDetals(articleId);
                    return article;
                }
            }
            catch (Exception e)
            {
                _log.Error(e);
                return null;
            }
        }

        //public SimpleArticle[] GetArticleList(int count, int page, ArticleOrder order)
        //{
        //    try
        //    {
        //        _log.InfoFormat("GetArticleList : count={0}, page ={1}", count, page);
        //        using (var proxy = new PersisterServiceClient())
        //        {
        //            SimpleArticle[] articles = proxy.GetArticleList(count, page, order);
        //            return articles;
        //        }
        //    }
        //    catch(Exception e)
        //    {
        //        _log.Error(e);
        //        return null;
        //    }
        //}

        public ArticleListModel GetArticleListModel(int count, int page, ArticleOrder order)
        {
            try
            {
                _log.InfoFormat("GetArticleList : count={0}, page ={1}", count, page);
                using (var proxy = new PersisterServiceClient())
                {
                    ArticleListModel model = proxy.GetArticleListModel(count, page, order);
                    return model;
                }
            }
            catch (Exception e)
            {
                _log.Error(e);
                return null;
            }
        }

        public string Test()
        {
            _log.Info("star test");
            return "test";
        }
    }
}
