﻿using System;
using ShopDiscount.Crawling.Starters;

namespace ShopDiscount.Crawling.Process
{
    class Program
    {
        static void Main(string[] args)
        {
            IStarter starter = new DatabaseStarter();
            try
            {
                starter.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadLine();
                Console.WriteLine("Press ENTER  to continue;");
                System.Diagnostics.Process.GetCurrentProcess().Kill();
                Environment.Exit(-1);
            }
        }
    }
}
