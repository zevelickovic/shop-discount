﻿using log4net;
using ShopDiscount.Models;
using ShopDiscount.Models.Responses;
using ShopDiscounts.Database.Exceptions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace ShopDiscounts.Database
{
    public class ShopDiscountDb
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly string _connectionKey;

        public ShopDiscountDb(string connectionKey)
        {
            _connectionKey = connectionKey;
        }

        #region Pull Request

        public List<Article> PullActiveArticles()
        {
            try
            {
                List<Article> articles = new List<Article>();
                using (var sqlConnection = new SqlConnection(DbConfig()))
                {
                    sqlConnection.Open();
                    using (var command = new SqlCommand("dbo.PullActiveArticles", sqlConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        var reader = command.ExecuteReader();

                        try
                        {
                            while (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                reader.GetValues(values);

                                var Id = Convert.ToInt64(values[0]);
                                var Identifier = (string)values[1];
                                var title = (string)values[2];
                                var description = (string)values[3];
                                var url = (string)values[4];
                                var shopId = Convert.ToInt32(values[5]);
                                var regularPrice = Convert.ToDecimal(values[6]);
                                var currentPrice = Convert.ToDecimal(values[7]);
                                var lastUpdatedAt = Convert.ToDateTime(values[9]);
                                var smallImageSvc = (string)values[10];
                                var largeImageSvc = (string)values[11];
                                var shopName = (string)values[12];
                                var shopUrl = (string)values[13];
                                var shopIsActive = (bool)values[14];
                                var shopAvatarSrc = (string)values[15];
                                var shopClass = (string)values[16];
                                var htmlBody = (string)values[17];

                                var article = new Article(new Shop(shopId, shopName, shopUrl, shopIsActive, shopAvatarSrc, shopClass))
                                {
                                    ArticleIdentifier = Identifier,
                                    ArticleId = Id,
                                    ArticleDetails = new ArticleDetails()
                                    {
                                        Description = description,
                                        Title = title,
                                        Url = url,
                                        HtmlBody = htmlBody
                                    },
                                    Image = new Image() { LargeImageSvc = largeImageSvc, SmallImageSvc = smallImageSvc},
                                    Price = new Price(currentPrice, regularPrice)
                                };

                                articles.Add(article);
                            }
                            return articles;
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                        finally
                        {
                            reader.Close();
                        }

                    }
                }
            }
            catch (Exception e)
            {
                Log.ErrorFormat("[Failed][TryAddOrUpdateArtcile] : {0}", e);
                return null;
            }
        }
        public List<Shop> GetShops()
        {
            try
            {
                List<Shop> shops = new List<Shop>();
                using (var sqlConnection = new SqlConnection(DbConfig()))
                {
                    sqlConnection.Open();
                    using (var command = new SqlCommand("dbo.GetShops", sqlConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        var reader = command.ExecuteReader();

                        try
                        {
                            while (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                reader.GetValues(values);

                                var shopId = Convert.ToInt32(values[0]);
                                var shopName = (string)values[1];
                                var shopUrl = (string)values[2];
                                var shopIsActive = (bool)values[3];
                                var shopAvatarSrc = (string)values[4];
                                var shopClass = (string)values[5];

                                var shop = new Shop(shopId, shopName, shopUrl, shopIsActive, shopAvatarSrc, shopClass);

                                shops.Add(shop);
                            }
                            return shops;
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                        finally
                        {
                            reader.Close();
                        }

                    }
                }
            }
            catch (Exception e)
            {
                Log.ErrorFormat("[Failed][GetShops] : {0}", e);
                return null;
            }
        }

        #endregion

        #region Article UpdateRequests

        public bool TryAddOrUpdateArtcile(Article article, out ArticleAddOrUpdateResponse response)
        {
            response = new ArticleAddOrUpdateResponse();
            try
            {
                using (var sqlConnection = new SqlConnection(DbConfig()))
                {
                    sqlConnection.Open();
                    using (var command = new SqlCommand("dbo.AddOrUpdateArticle", sqlConnection))
                    {
                        command.Parameters.AddWithValue("@articleIdentifier", article.ArticleIdentifier);
                        command.Parameters.AddWithValue("@title", article.ArticleDetails.Title);
                        command.Parameters.AddWithValue("@description", article.ArticleDetails.Description);
                        command.Parameters.AddWithValue("@url", article.ArticleDetails.Url);
                        command.Parameters.AddWithValue("@shopId", article.Shop.ShopId);
                        command.Parameters.AddWithValue("@regularPrice", article.Price.RegularPrice);
                        command.Parameters.AddWithValue("@currentPrice", article.Price.CurrentPrice);
                        command.Parameters.AddWithValue("@persentage", article.Price.Persentage);
                        command.Parameters.AddWithValue("@lastUpdatedAt", article.Price.LastUpdatedAt);
                        command.Parameters.AddWithValue("@smallImageSvc", article.Image.SmallImageSvc);
                        command.Parameters.AddWithValue("@largeImageSvc", article.Image.LargeImageSvc);
                        command.Parameters.AddWithValue("@htmlBody", article.ArticleDetails.HtmlBody);
                        command.Parameters.Add("@isNew", SqlDbType.Bit).Direction = ParameterDirection.Output;
                        command.Parameters.Add("@articleId", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                        command.CommandType = CommandType.StoredProcedure;

                        command.ExecuteNonQuery();

                        var articleId = Convert.ToInt64(command.Parameters["@articleId"].Value);
                        var isNew = (bool)command.Parameters["@isNew"].Value;

                        response = new ArticleAddOrUpdateResponse(articleId, isNew);

                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                Log.ErrorFormat("[Failed][TryAddOrUpdateArtcile] : {0}", e);
                return false;
            }
        }

        #endregion

        #region Connection String Handling

        private string DbConfig()
        {
            var conn = ConfigurationManager.ConnectionStrings[_connectionKey];
            if (conn == null)
                throw new MissingDatabaseConfigurationExeption(string.Format("Missing {0} connection string in config file", _connectionKey));
            return conn.ConnectionString;
        }

        #endregion
    }
}