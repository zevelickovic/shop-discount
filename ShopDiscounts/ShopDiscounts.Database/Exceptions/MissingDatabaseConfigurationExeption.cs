﻿using System;

namespace ShopDiscounts.Database.Exceptions
{
    class MissingDatabaseConfigurationExeption : Exception
    {
        public MissingDatabaseConfigurationExeption(string message) : base(message)
        {

        }
    }
}
