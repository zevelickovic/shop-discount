﻿using ShopDiscounts.Proxies.PersisterServiceReference;
using System;
using System.Collections.Generic;

namespace ShopDiscount.Hardcoded
{
    public static class Fake
    {
        private static Article NewArticle(Shop shop)
        {
            var article = new Article() {
                Shop = shop,
                ArticleDetails = new ArticleDetails()
                {
                    Description = "Nekakav opis " + Guid.NewGuid(),
                    Title = "Title " + Guid.NewGuid(),
                    Url = "http://" + Guid.NewGuid() + ".rs"
                },
                Price = new Price()
                {
                    CurrentPrice = 199.82m,
                    RegularPrice = 265.89m,
                    LastUpdatedAt = DateTime.UtcNow
                },
                Image = new Image()
                {
                    LargeImageSvc = "http://www.winwin.rs/auto-sediste-nania-beline-sp-9-36kg-1-2-3-rock-gray-tamno-siva-5100057-6781431.html",
                    SmallImageSvc = "http://www.winwin.rs/auto-sediste-nania-beline-sp-9-36kg-1-2-3-rock-gray-tamno-siva-5100057-6781431.html"
                }
             };
            return article;
        }
        public static List<Article> CreateArticles(Shop shop)
        {
            var articles = new List<Article>();
            for(var i = 0; i < 10; i++ )
            {
                var article = NewArticle(shop);
                article.ArticleIdentifier = "Article" + i;
                articles.Add(article);
            }
            return articles;
        }
    }
}