﻿using ShopDiscount.Models;
using ShopDiscount.Models.Client;
using ShopDiscount.Models.Helpers;
using ShopDiscounts.Cash.Hardcoded;
using ShopDiscounts.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShopDiscounts.Cash
{
    public static class CashHolder
    {
        private static readonly Dictionary<long, object> ArticlesLock = new Dictionary<long, object>();
        private static readonly Dictionary<long, SimpleArticle> SimpleArticles = new Dictionary<long, SimpleArticle>();
        private static readonly Dictionary<long, Article> Articles = new Dictionary<long, Article>();

        private static readonly Dictionary<string, long> ArticlesIds = new Dictionary<string, long>();
        private static readonly Dictionary<long, string> ArticlesHash = new Dictionary<long, string>();
        private static readonly Dictionary<long, DateTime> ArticlesUpdate = new Dictionary<long, DateTime>();

        private static readonly object ObjLock = new object();

        #region Articles Cash Modifiers

        private static long GetArticleId(string articleKey)
        {
            long articleId;
            if (ArticlesIds.TryGetValue(articleKey, out articleId))
                return articleId;
            return -1;
        }

        public static void InsertArticle(Article article)
        {
            lock(ObjLock)
            {
                var articleId = GetArticleId(article.ArticleKey);
                if (articleId > 0)
                {
                    UpdateArticle(article);
                    return;
                }
                Articles.Add(article.ArticleId, article);
                SimpleArticles.Add(article.ArticleId, article.Simplify());
                ArticlesIds.Add(article.ArticleKey, article.ArticleId);
                ArticlesLock.Add(article.ArticleId, new object());
                ArticlesUpdate.Add(article.ArticleId, DateTime.UtcNow);
                ArticlesHash.Add(article.ArticleId, article.ToSHA1Hash());
            }
        }

        public static void UpdateArticle(Article article)
        {
            lock (ArticlesLock[article.ArticleId])
            {
                if(ArticlesHash[article.ArticleId].Equals(article.ToSHA1Hash()))
                {
                    RefreshArticle(article);
                    return;
                }
                Articles[article.ArticleId] = article;
                SimpleArticles[article.ArticleId] = article.Simplify();
                ArticlesUpdate[article.ArticleId] = DateTime.UtcNow;
                ArticlesHash[article.ArticleId] = article.ToSHA1Hash();
            }
        }

        private static void RefreshArticle(Article article)
        {
            ArticlesUpdate[article.ArticleId] = DateTime.UtcNow;
        }

        #endregion

        #region Datebase Pull

        public static bool PullFromDatabaseHardCodedTest()
        {
            if (Articles.Count != 0) return true;
            foreach (var article in Fake.CreateArticles(new Shop(1, "neki shop", "http://neki.rs", true, "http://neki.rs", "http://neki.rs")))
            {
                InsertArticle(article);
            }
            return true;
        }
        public static bool PullFromDatabase()
        {
            try
            {
                if(Articles.Count == 0)
                    lock (ObjLock)
                    {
                        if (Articles.Count != 0) return true;

                        var db = new ShopDiscountDb("ShopDiscounts");
                        var articles = db.PullActiveArticles();
                        foreach (var article in articles)
                            InsertArticle(article);
                    }
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        #endregion

        #region Cash Readers

        public static Article GetArticleDetals(long articleId)
        {
            try
            {
                return Articles[articleId];
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static IEnumerable<SimpleArticle> GetArticleList(int count, int page, ArticleOrder order)
        {
            try
            {
                IEnumerable<SimpleArticle> query;
                lock (ObjLock)
                {
                    switch (order)
                    {
                        case ArticleOrder.Title:
                            query = from a in SimpleArticles.Values
                                    orderby a.ShortTitle ascending
                                    select a;
                            break;
                        case ArticleOrder.CurrentPrice:
                            query = from a in SimpleArticles.Values
                                    orderby a.CurrentPrice ascending
                                    select a;
                            break;
                        case ArticleOrder.RegularPrice:
                            query = from a in SimpleArticles.Values
                                    orderby a.RegularPrice ascending
                                    select a;
                            break;
                        case ArticleOrder.Persentage:
                            query = from a in SimpleArticles.Values
                                    orderby a.Persentage ascending
                                    select a;
                            break;
                        default:
                            return new List<SimpleArticle>();
                    }
                }
                return query.Skip(page * count).Take(count);
            }
            catch (Exception)
            {
                return new List<SimpleArticle>();
            }
        }
        public static ArticleListModel GetArticleListModel(int count, int page, ArticleOrder order)
        {
            ArticleListModel model = new ArticleListModel(count, page, (int)order);

            try
            {
                IEnumerable<SimpleArticle> query;
                lock (ObjLock)
                {
                    switch (order)
                    {
                        case ArticleOrder.Title:
                            query = from a in SimpleArticles.Values
                                    orderby a.ShortTitle ascending
                                    select a;
                            break;
                        case ArticleOrder.CurrentPrice:
                            query = from a in SimpleArticles.Values
                                    orderby a.CurrentPrice ascending
                                    select a;
                            break;
                        case ArticleOrder.RegularPrice:
                            query = from a in SimpleArticles.Values
                                    orderby a.RegularPrice ascending
                                    select a;
                            break;
                        case ArticleOrder.Persentage:
                            query = from a in SimpleArticles.Values
                                    orderby a.Persentage ascending
                                    select a;
                            break;
                        default:
                            return model;
                    }
                }
                model.Pagination.TotalPages = ResolveTotalPages(count, query.Count());
                model.Articles = query.Skip(page * count).Take(count).ToArray();
                return model;
            }
            catch (Exception)
            {
                return model;
            }
        }

        private static int ResolveTotalPages(int count, int totalArticles)
        {
            var pages = totalArticles / count;
            if (totalArticles % count > 0)
                pages++;
            return pages;
        }
        #endregion
    }
}
