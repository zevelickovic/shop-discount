﻿using ShopDiscount.Models;
using System.Collections.Generic;

namespace ShopDiscounts.Cash
{
    internal interface ICashWrapper
    {
        long GetArticleId(string articleIdentifier, int shopId);
        void UpdateArticle(Article article);
        void InsertArticle(Article article);
        IEnumerable<Article> GetArticles(int page);
    }
}
