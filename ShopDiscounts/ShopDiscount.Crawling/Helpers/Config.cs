﻿using ShopDiscounts.Proxies.PersisterServiceReference;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;

namespace ShopDiscount.Crawling.Helpers
{
    public static class Config
    {
        public static string[] GetArgsFromConfig()
        {
            var shopConfig = ConfigurationManager.GetSection("shop") as NameValueCollection;
            var args = new List<string>();

            args.Add(shopConfig["shopId"]);
            args.Add(shopConfig["name"]);
            args.Add(shopConfig["url"]);
            args.Add(shopConfig["isActive"]);
            args.Add(shopConfig["avatarSrc"]);
            args.Add(shopConfig["classType"]);

            return args.ToArray();
        }

        public static Shop GetShopModelFromArgs(this string[] args)
        {
            var shopId = int.Parse(args[0]);
            var name = args[1];
            var url = args[2];
            var isActive = bool.Parse(args[3]);
            var avatarSrc = args[4];
            var classType = args[5];
            return new Shop()
            {
                ShopId = shopId,
                Name = name,
                Url = url,
                IsActive = isActive,
                AvatarSvc = avatarSrc,
                ClassType = classType
            };
        }
    }
}