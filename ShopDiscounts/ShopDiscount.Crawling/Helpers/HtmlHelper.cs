﻿using HtmlAgilityPack;
using System.Net;
using System.Text.RegularExpressions;

namespace ShopDiscount.Crawling.Helpers
{
    public static class HtmlHelper
    {
        public static string GetLink(this HtmlNode aTag)
        {
            return aTag.Attributes["href"].Value;
        }
        public static string GetImageSrc(this HtmlNode aTag)
        {
            return aTag.Attributes["src"].Value;
        }
        public static string GetText(this HtmlNode node)
        {
            return node.InnerText.CleanUp();
        }

        public static string CleanUp(this string str)
        {
            var result = WebUtility.HtmlDecode(str);
            result = result.HtmlTrim()
                .Replace("\n", string.Empty)
                .Replace("\r", string.Empty)
                .Replace("\t", " ")
                .EncodeHtmlCharacters()
                .CleanMultipleSpaces()
                .Trim();

            return result;
        }
        public static string HtmlCleaner(this string html)
        {
            return html.HtmlTrim().CleanUp().Replace("> <", "><");
        }
        private static string EncodeHtmlCharacters(this string str)
        {
            var result = str.HtmlTrim()
               .Replace("&nbsp;", " ")
               .Replace("&quot;", "\"")
               .Replace("&nbsp", "");
            return result;
        }
        private static string HtmlTrim(this string str)
        {
            return str.Trim().Trim('\n').Trim('\r').Trim('\t');
        }
        private static string CleanMultipleSpaces(this string str)
        {
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex(@"[ ]{2,}", options);
            str = regex.Replace(str, @" ");
            return str;
        }
        public static string RemoveAllAttributesFromEveryNode(string html)
        {
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html);
            foreach (var eachNode in htmlDocument.DocumentNode.SelectNodes("//*"))
                eachNode.Attributes.RemoveAll();
            html = htmlDocument.DocumentNode.OuterHtml;
            return html;
        }

        public static string RemoveAttributesFromTableHtml(this string table)
        {
            var result = Regex.Replace(table, @"<table[\s\S]*?>", "<table>");
            result = Regex.Replace(result, @"<th[\s\S]*?>", "<th>");
            result = Regex.Replace(result, @"<td[\s\S]*?>", "<td>");
            result = Regex.Replace(result, @"<col[\s\S]*?>", string.Empty);
            result = result.Replace("<tbody>", string.Empty).Replace("</tbody>", string.Empty);
            return result;
        }
    }
}
