﻿using System.Configuration;

namespace ShopDiscount.Crawling.Configuration
{
    public class ShopConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }
        [ConfigurationProperty("shopId",  IsRequired = true)]
        public int ShopId
        {
            get { return (int)this["shopId"]; }
            set { this["shopId"] = value; }
        }
        [ConfigurationProperty("classType", IsRequired = true)]
        public string ClassType
        {
            get { return (string)this["classType"]; }
            set { this["classType"] = value; }
        }
        [ConfigurationProperty("isActive", IsRequired = true)]
        public bool IsActive
        {
            get { return (bool)this["isActive"]; }
            set { this["isActive"] = value; }
        }
        [ConfigurationProperty("avatarSrc", IsRequired = true)]
        public string AvatarSrc
        {
            get { return (string)this["avatarSrc"]; }
            set { this["avatarSrc"] = value; }
        }
    }
}
