﻿using System.Configuration;

namespace ShopDiscount.Crawling.Configuration
{
    public class ShopConfigurationElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ShopConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ShopConfigurationElement)element).Name;
        }

        //Returns the configuration element with the specified key.
        new public ShopConfigurationElement this[string Name]
        {
            get
            {
                return (ShopConfigurationElement)BaseGet(Name);
            }
        }
    }
}
