﻿using System.Configuration;

namespace ShopDiscount.Crawling.Configuration
{
    public class ShopConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("shops", IsRequired = false)]
        public ShopConfigurationElementCollection Site
        {
            get
            {
                return (ShopConfigurationElementCollection)this["shops"];
            }
            set
            { this["shops"] = value; }
        }
    }
}
