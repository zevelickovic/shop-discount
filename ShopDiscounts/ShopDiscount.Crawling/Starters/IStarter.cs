﻿namespace ShopDiscount.Crawling.Starters
{
    public interface IStarter
    {
        void Start();
    }
}
