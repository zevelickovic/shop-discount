﻿using ShopDiscount.Crawling.Helpers;
using ShopDiscount.Crawling.Parsers;
using System;

namespace ShopDiscount.Crawling.Starters
{
    public class ConfigurationsStarter : IStarter
    {
        public void Start()
        {
            var args = Config.GetArgsFromConfig();

            var model = args.GetShopModelFromArgs();
            var type = Type.GetType(model.ClassType);
            if (type == null) return;
            var parser = Activator.CreateInstance(type, model) as IParser;
            parser.DoParse();
        }
    }
}
