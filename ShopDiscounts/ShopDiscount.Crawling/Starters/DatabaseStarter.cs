﻿using ShopDiscount.Crawling.Parsers;
using ShopDiscounts.Proxies;
using System;

namespace ShopDiscount.Crawling.Starters
{
    public class DatabaseStarter : IStarter
    {
        private IPersisterProxy _proxy = new PersisterProxy();

        public void Start()
        {
            var shops = _proxy.GetShops();

            foreach (var shop in shops)
            {
                var type = Type.GetType(shop.ClassType);
                if (type == null) return;
                var parser = Activator.CreateInstance(type, shop) as IParser;
                parser.DoParse();
            }
        }
    }
}
