﻿using ShopDiscount.Crawling.Browsers;
using ShopDiscounts.Proxies;
using ShopDiscounts.Proxies.PersisterServiceReference;
using System;
using System.Collections.Generic;

namespace ShopDiscount.Crawling.Parsers
{
    public abstract class Parser : IParser
    {
        private Shop _shop;
        private IPersisterProxy _persister = new PersisterProxy();
        protected WebClientAgent Agent { get; }

        public Parser(Shop shop)
        {
            Agent = new WebClientAgent();
            _shop = shop;
        }

        public void DoParse()
        {
            var articles = Parse();
            if(articles != null)
            _persister.BulkInsertArticles(articles);
        }

        protected abstract IEnumerable<Article> Parse();

        protected Article CreateArticle(string idenrifier, string url, string title, string description, decimal currentPrice, decimal regularPrice, string smallImageSvc, string largeImageSvc, string htmlBody)
        {
            Article article = new Article()
            {
                ArticleIdentifier = idenrifier,
                ArticleDetails = new ArticleDetails()
                {
                    Description = description,
                    Title = title,
                    Url = url,
                    HtmlBody = htmlBody
                },
                Image = new Image()
                {
                    SmallImageSvc = smallImageSvc,
                    LargeImageSvc = largeImageSvc
                },
                Price = new Price()
                {
                    CurrentPrice = currentPrice,
                    RegularPrice = regularPrice,
                    LastUpdatedAt = DateTime.UtcNow
                },
                Shop = _shop
            };
            return article;
        }
    }
}