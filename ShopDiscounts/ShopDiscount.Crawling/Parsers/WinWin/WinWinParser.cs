﻿using ShopDiscounts.Proxies.PersisterServiceReference;
using System.Collections.Generic;
using System.Net;
using ShopDiscount.Crawling.Helpers;
using System;
using HtmlAgilityPack;

namespace ShopDiscount.Crawling.Parsers.WinWin
{
    public class WinWinParser : Parser
    {
        public WinWinParser(Shop shop) : base(shop)
        {
            Agent.Headers.Add("Host", "www.winwin.rs");
        }

        protected override IEnumerable<Article> Parse()
        {
            try
            {
                List<Article> articles = new List<Article>();

                Agent.Headers.Add(HttpRequestHeader.Referer, "http://www.winwin.rs/");

                var requestUrl = "http://www.winwin.rs/filters/product/action/";
                var html = Agent.DownloadString(requestUrl);
                Console.WriteLine("Requested : " + requestUrl);

                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(html);

                var nodes = htmlDoc.DocumentNode.SelectNodes(".//ul[@id='categs_id']/li[p[@class='laptop-i-tablet-racunari']]/ul/li/a");
                foreach (var node in nodes)
                {
                    var text = node.GetText();
                    var link = node.GetLink();

                    articles.AddRange(CollectArticles(link, "http://www.winwin.rs/filters/product/action/"));
                }

                Console.WriteLine("Collected : " + articles.Count + " articles"); 

                return articles;
            }
            catch(Exception)
            {
                return null;
            }
        }

        #region Crowling Methods

        private IEnumerable<Article> CollectArticles(string link, string referer)
        {
            List<Article> articles = new List<Article>();
            Agent.Headers.Set(HttpRequestHeader.Referer, referer);

            var html = Agent.DownloadString(link);
            Console.WriteLine("Requested : " + link);

            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);

            var nodes = htmlDoc.DocumentNode.SelectNodes(".//ol[@class='products-list']/li[@itemprop='itemListElement']");
            foreach(var node in nodes)
            {
                var article = ResolveArticle(node, link);
                if(article != null)
                    articles.Add(article);
            }

            string nextPageLink = CheckForNextPageLink(htmlDoc);
            if (nextPageLink != null)
                articles.AddRange(CollectArticles(nextPageLink, link));
            return articles;
        }

        private string CheckForNextPageLink(HtmlDocument htmlDoc)
        {
            try
            {
                return htmlDoc.DocumentNode.SelectSingleNode(".//a[@class='next i-next']").GetLink().CleanUp();
            }
            catch(Exception)
            {
                return null;
            }
        }

        private Article ResolveArticle(HtmlNode node, string referer)
        {
            try
            {
                var linkNode = node.SelectSingleNode(".//a[@itemprop='name']");
                var url = linkNode.GetLink();
                var title = linkNode.GetText();

                var imageNode = node.SelectSingleNode(".//img[@itemprop='image']");
                var smallImageSvc = imageNode.GetImageSrc();

                var regularPriceNode = node.SelectSingleNode(".//span[starts-with(@id, 'strike-price') or starts-with(@id,'mp-price')]");
                var regularPriceString = string.Format("{0}.00", regularPriceNode.GetText().Replace("din", "").Replace(".", "").CleanUp());
                var regularPrice = Convert.ToDecimal(regularPriceString);

                var currentPriceNode = node.SelectSingleNode(".//span[@class='price']");
                var currentPriceString = string.Format("{0}.00", currentPriceNode.GetText().Replace("din", "").Replace(".", "").CleanUp());
                var currentPrice = Convert.ToDecimal(currentPriceString);

                var identifierNode = node.SelectSingleNode(".//li[strong[text()='Šifra artikla:']]");
                var idenrifier = identifierNode.GetText().Replace("Šifra artikla:", "").CleanUp();

                Tuple<string, string, string> tuple = CollectArticleDetails(url, referer);

                var largeImageSvc = tuple.Item1;
                var description = tuple.Item2;
                var htmlBody = tuple.Item3.HtmlCleaner().RemoveAttributesFromTableHtml();

               return CreateArticle(idenrifier, url, title, description, currentPrice, regularPrice, smallImageSvc, largeImageSvc, htmlBody);
            }
            catch(Exception)
            {
                return null;
            }
        }

        private Tuple<string, string, string> CollectArticleDetails(string link, string referer)
        {
            List<Article> articles = new List<Article>();
            Agent.Headers.Set(HttpRequestHeader.Referer, referer);

            var html = Agent.DownloadString(link);
            Console.WriteLine("Requested : " + link);

            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);

            Tuple<string, string, string>  tuple = ResolveArticle(htmlDoc);

            return tuple;
        }

        private Tuple<string, string, string> ResolveArticle(HtmlDocument htmlDoc)
        {
            Tuple<string, string, string> tuple = null;

            var largeImageSvc = htmlDoc.DocumentNode.SelectSingleNode(".//a[@data-full]").Attributes["data-full"].Value;
            var description = htmlDoc.DocumentNode.SelectSingleNode(".//div[@itemprop='description']").GetText();
            var htmlBody = htmlDoc.DocumentNode.SelectSingleNode(".//table[@id='product-attribute-specs-table']").OuterHtml;

            tuple = new Tuple<string, string, string>(largeImageSvc, description, htmlBody);

            return tuple;
        }
        #endregion
    }
}