﻿using ShopDiscounts.Proxies.PersisterServiceReference;
using System.Collections.Generic;
using System.Net;
using ShopDiscount.Crawling.Helpers;
using System;
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace ShopDiscount.Crawling.Parsers.Tehnomanija
{
    public class TehnomanijaParser : Parser
    {
        public TehnomanijaParser(Shop shop) : base(shop)
        {
            Agent.Headers.Add("Host", "www.tehnomanija.rs");
        }

        protected override IEnumerable<Article> Parse()
        {
            try
            {
                List<Article> articles = new List<Article>();
                
                Agent.Headers.Add(HttpRequestHeader.Referer, "http://www.tehnomanija.rs/akcija.html");

                var requestUrl = "http://www.tehnomanija.rs/index.php?mod=catalog&op=browse&view=promo&filters%5Bpromo%5D%5B%5D=IT+rasprodaja&refreshCheck=1";
                var html = Agent.DownloadString(requestUrl);
                Console.WriteLine("Requested : " + requestUrl);

                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(html);

                var nodes = htmlDoc.DocumentNode.SelectNodes(".//div[@itemprop='itemListElement']");
                foreach (var node in nodes)
                {
                    var article = ResolveArticle(node, requestUrl);
                    if(article != null)
                        articles.Add(article);
                }

                Console.WriteLine("Collected : " + articles.Count + " articles");

                return articles;
            }
            catch (Exception)
            {
                return null;
            }
        }

        #region Crowling Methods

        private Article ResolveArticle(HtmlNode node, string referer)
        {
            try
            {
                var linkNode = node.SelectSingleNode(".//a[@itemprop='name']");
                var url = "http:" + linkNode.GetLink();
                var title = linkNode.GetText();

                var imageNode = node.SelectSingleNode(".//img[@itemprop='image']");
                var smallImageSvc = "http:" + imageNode.GetImageSrc();

                var currentPriceNode = node.SelectSingleNode(".//span[@itemprop='price']");
                var currentPriceString = string.Format("{0}.00", currentPriceNode.InnerHtml.Split('\n')[0].Replace("rsd","").Replace(".","").CleanUp());
                var currentPrice = Convert.ToDecimal(currentPriceString);

                var savingNode = node.SelectSingleNode(".//span[@class='savings_value']");
                if (savingNode == null)
                    return null;
                var savingString = string.Format("{0}.00", savingNode.GetText().Replace("rsd", "").Replace(".", "").Replace("-", "").CleanUp());
                var saving = Convert.ToDecimal(savingString);
                var regularPrice = currentPrice + saving;

                var identifierNode = node.SelectSingleNode(".//div[@class='product-basket']/a");
                if (identifierNode == null)
                    identifierNode = node.SelectSingleNode(".//a[@class='outofstocktxt lager']");///kontakt/77900
                var identifierLink = identifierNode.GetLink();

                var idenrifier =  new Regex(@"(&product_id.)(\d+)").Match(identifierLink).Groups[2].Value.CleanUp();
                if(string.IsNullOrEmpty(idenrifier))
                    idenrifier = new Regex(@"(/kontakt/)(\d+)").Match(identifierLink).Groups[2].Value.CleanUp();
                if (string.IsNullOrEmpty(idenrifier))
                    return null;

                Tuple<string, string, string> tuple = CollectArticleDetails(url, referer);

                var largeImageSvc = tuple.Item1;
                var description = tuple.Item2;
                var htmlBody = tuple.Item3.HtmlCleaner().RemoveAttributesFromTableHtml();

                return CreateArticle(idenrifier, url, title, description, currentPrice, regularPrice, smallImageSvc, largeImageSvc, htmlBody);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private Tuple<string, string, string> CollectArticleDetails(string link, string referer)
        {
            List<Article> articles = new List<Article>();
            Agent.Headers.Set(HttpRequestHeader.Referer, referer);

            var html = Agent.DownloadString(link);
            Console.WriteLine("Requested : " + link);

            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);

            Tuple<string, string, string> tuple = ResolveArticle(htmlDoc);

            return tuple;
        }

        private Tuple<string, string, string> ResolveArticle(HtmlDocument htmlDoc)
        {
            Tuple<string, string, string> tuple = null;

            var largeImageSvc = "http:" + htmlDoc.DocumentNode.SelectSingleNode(".//a[@itemprop='image']").GetLink();
            var description = htmlDoc.DocumentNode.SelectSingleNode(".//div[@class='short_description']").GetText().Replace("•", "\n•").Trim();
            var htmlBodyNode = htmlDoc.DocumentNode.SelectSingleNode(".//div[@id='product-specs']");
            var htmlBody = htmlBodyNode.SelectSingleNode(".//table").OuterHtml.HtmlCleaner();

            tuple = new Tuple<string, string, string>(largeImageSvc, description, htmlBody);

            return tuple;
        }
        #endregion
    }
}