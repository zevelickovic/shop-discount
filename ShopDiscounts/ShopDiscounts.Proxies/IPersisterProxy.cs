﻿using ShopDiscounts.Proxies.PersisterServiceReference;
using System.Collections.Generic;

namespace ShopDiscounts.Proxies
{
    public interface IPersisterProxy
    {
        void BulkInsertArticles(IEnumerable<Article> articles);
        List<Shop> GetShops();
    }
}
