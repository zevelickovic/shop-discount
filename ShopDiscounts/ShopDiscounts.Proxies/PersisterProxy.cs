﻿using System.Collections.Generic;
using ShopDiscounts.Proxies.PersisterServiceReference;
using System.Linq;
using System;

namespace ShopDiscounts.Proxies
{
    public class PersisterProxy : IPersisterProxy
    {
        public void BulkInsertArticles(IEnumerable<Article> articles)
        {
            using (var client = new PersisterServiceClient())
            {
                client.BulkInsertArticles(articles.ToList());
            }
        }
        public List<Shop> GetShops()
        {
            try
            {
                using (var client = new PersisterServiceClient())
                {
                    return client.GetShops().ToList();
                }
            }
            catch(Exception)
            {
                return new List<Shop>();
            }
        }
    }
}
