﻿using System.Collections.Generic;

namespace ShopDiscounts.Proxies.Wrappers
{
    public static class ArticlesWrapper
    {
        public static List<PersisterServiceReference.Article> ToProxyArticleList(this IEnumerable<ShopDiscount.Models.Article> articles)
        {
            List<PersisterServiceReference.Article> list = new List<PersisterServiceReference.Article>();

            foreach (var a in articles)
                list.Add(a.ToProxyArticle());

            return list;
        }

        private static PersisterServiceReference.Article ToProxyArticle(this ShopDiscount.Models.Article article)
        {
            return new PersisterServiceReference.Article()
            {
                Shop = new PersisterServiceReference.Shop()
                {
                    AvatarSvc = article.Shop.AvatarSvc,
                    ClassType = article.Shop.ClassType,
                    IsActive = article.Shop.IsActive,
                    Name = article.Shop.Name,
                    ShopId = article.Shop.ShopId,
                    Url = article.Shop.Url
                },
                ArticleDetails = new PersisterServiceReference.ArticleDetails()
                {
                    Description = article.ArticleDetails.Description,
                    ShortTitle = article.ArticleDetails.ShortTitle,
                    Title = article.ArticleDetails.Title,
                    Url = article.ArticleDetails.Url,
                    Status = article.ArticleDetails.Status
                },
                Price = new PersisterServiceReference.Price()
                {
                    CurrentPrice = article.Price.CurrentPrice,
                    LastUpdatedAt = article.Price.LastUpdatedAt,
                    Persentage = article.Price.Persentage,
                    RegularPrice = article.Price.RegularPrice
                },
                Image = new PersisterServiceReference.Image()
                {
                    LargeImageSvc = article.Image.LargeImageSvc,
                    SmallImageSvc = article.Image.SmallImageSvc
                }
            };
        }
    }
}
