﻿using System.Runtime.Serialization;

namespace ShopDiscount.Models
{
    [DataContract]
    public class SimpleArticle
    {
        [DataMember]
        public long ArticleId { get; set; }
        [DataMember]
        public string ShortTitle { get; set; }
        [DataMember]
        public string SmallAvatarSvc { get; set; }
        [DataMember]
        public string ShortDescription { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int ShopId { get; set; }
        [DataMember]
        public string ShopName { get; set; }
        [DataMember]
        public string ShopAvatarSvc { get; set; }
        [DataMember]
        public decimal CurrentPrice { get; set; }
        [DataMember]
        public decimal RegularPrice { get; set; }
        [DataMember]
        public decimal Persentage { get ; set; } 
    }
}
