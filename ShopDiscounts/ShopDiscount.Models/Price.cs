﻿using System;
using System.Runtime.Serialization;

namespace ShopDiscount.Models
{
    [DataContract]
    public class Price
    {
        [DataMember]
        public decimal RegularPrice { get; private set; }
        [DataMember]
        public decimal CurrentPrice { get; private set; }
        [DataMember]
        public DateTime LastUpdatedAt { get; private set; }
        public int Persentage { get { return (int)((RegularPrice - CurrentPrice) * 100 / RegularPrice); } }
        public Price()
        {

        }
        public Price(decimal currentPrice, decimal regularPrice)
        {
            CurrentPrice = currentPrice;
            RegularPrice = regularPrice;
            LastUpdatedAt = DateTime.UtcNow;
        }
        public Price(Price price)
        {
            CurrentPrice = price.CurrentPrice;
            RegularPrice = price.RegularPrice;
            LastUpdatedAt = price.LastUpdatedAt;
        }
        public override string ToString()
        {
            return string.Format("[{0}][{1}]", RegularPrice, CurrentPrice);
        }
    }
}
