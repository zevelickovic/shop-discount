﻿using System.Runtime.Serialization;

namespace ShopDiscount.Models
{
    [DataContract]
    public class ArticleDetails
    {
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string HtmlBody { get; set; }
        [DataMember]
        public string Url { get; set; }
        public ArticleDetails()
        {

        }
        public ArticleDetails(ArticleDetails articleDetals)
        {
            Title = articleDetals.Title;
            Description = articleDetals.Description;
            Url = articleDetals.Url;
            HtmlBody = articleDetals.HtmlBody;
        }
        public override string ToString()
        {
            return string.Format("[{0}][{1}][{2}][{3}]", Title, Description, Url, HtmlBody);
        }
    }
}
