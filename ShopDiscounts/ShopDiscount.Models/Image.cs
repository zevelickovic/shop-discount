﻿using System.Runtime.Serialization;

namespace ShopDiscount.Models
{
    [DataContract]
    public class Image
    {
        [DataMember]
        public string SmallImageSvc { get; set; }
        [DataMember]
        public string LargeImageSvc { get; set; }
        public Image()
        {
        }
        public Image(Image image)
        {
            SmallImageSvc = image.SmallImageSvc;
            LargeImageSvc = image.LargeImageSvc;
        }
        public override string ToString()
        {
            return string.Format("[{0}][{1}]", SmallImageSvc, LargeImageSvc);
        }
    }
}
