﻿using System.Runtime.Serialization;

namespace ShopDiscount.Models
{
    [DataContract]
    public class Shop
    {
        [DataMember]
        public int ShopId { get; private set; }
        [DataMember]
        public string Name { get; private set; }
        [DataMember]
        public string Url { get; private set; }
        [DataMember]
        public bool IsActive { get; private set; }
        [DataMember]
        public string AvatarSvc { get; private set; }
        [DataMember]
        public string ClassType { get; private set; }
        public Shop()
        {

        }
        public Shop(int shopId, string name, string url, bool isActive, string avatarSvc, string classType)
        {
            ShopId = shopId;
            Name = name;
            Url = url;
            IsActive = isActive;
            AvatarSvc = avatarSvc;
            ClassType = classType;
        }
        public Shop(Shop shop)
        {
            ShopId = shop.ShopId;
            Name = shop.Name;
            Url = shop.Url;
            IsActive = shop.IsActive;
            AvatarSvc = shop.AvatarSvc;
            ClassType = shop.ClassType;
        }
        public override string ToString()
        {
            return string.Format("[{0}][{1}][{2}][{3}]", ShopId, Name, Url, AvatarSvc);
        }
    }
}
