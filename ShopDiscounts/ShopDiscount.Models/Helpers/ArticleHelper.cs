﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ShopDiscount.Models.Helpers
{
    public static class ArticleHelper
    {
        public static SimpleArticle Simplify(this Article article)
        {
            var simpleArticle = new SimpleArticle()
            {
                ArticleId = article.ArticleId,
                Url = article.ArticleDetails.Url,
                ShortTitle = article.GetShortTitle(),
                SmallAvatarSvc = article.Image.SmallImageSvc,
                ShopAvatarSvc = article.Shop.AvatarSvc,
                ShortDescription = article.ArticleDetails.Description.TrancateString(50),
                ShopId = article.Shop.ShopId,
                ShopName = article.Shop.Name,
                CurrentPrice = article.Price.CurrentPrice,
                RegularPrice = article.Price.RegularPrice,
                Persentage = article.Price.Persentage
            };

            return simpleArticle;
        }
        private static string GetShortTitle(this Article article)
        {
            return article.ArticleDetails.Title.TrancateString(25);
        }
        private static string TrancateString(this string str, int index)
        {
            if (str.Length > index)
                str = string.Format("{0}...", str.Substring(0, index));
            return str;
        }
        #region SHA1 Hash string

        public static string ToSHA1Hash(this Article article)
        {
            return article.ToString().SHA1HashStringForUTF8String();
        }

        private static string SHA1HashStringForUTF8String(this string input)
        {
            var hashString = string.Empty;

            try
            {
                using (var sha1 = SHA1.Create())
                {
                    byte[] bytes = Encoding.UTF8.GetBytes(input);

                    byte[] hashBytes = sha1.ComputeHash(bytes);

                    hashString = HexStringFromBytes(hashBytes);
                }
            }
            catch (Exception)
            {
                
            }

            return hashString;
        }

        private static string HexStringFromBytes(byte[] bytes)
        {
            var stringBuilder = new StringBuilder();
            foreach (byte b in bytes)
            {
                var hex = b.ToString("x2");
                stringBuilder.Append(hex);
            }
            return stringBuilder.ToString();
        }
        #endregion
    }
}