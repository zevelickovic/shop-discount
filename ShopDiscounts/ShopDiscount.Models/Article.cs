﻿using System;
using System.Runtime.Serialization;

namespace ShopDiscount.Models
{
    [DataContract]
    public class Article
    {
        [DataMember]
        public long ArticleId { get; set; }
        [DataMember]
        public string ArticleIdentifier { get; set; }
        [DataMember]
        public ArticleDetails ArticleDetails { get; set; }
        [DataMember]
        public Shop Shop { get; private set; }
        [DataMember]
        public Image Image { get; set; }
        [DataMember]
        public Price Price { get; set; }
        public string ArticleKey
        {
            get { return string.Format("{0}-{1}", Shop.ShopId, ArticleIdentifier); }
        }
        public bool IsActive
        {
            get { return DateTime.Compare(DateTime.UtcNow.AddHours(1), Price.LastUpdatedAt) > 0; }
        }
        public Article()
        {
            Shop = new Shop();
            ArticleDetails = new ArticleDetails();
            Image = new Image();
            Price = new Price();
        }
        public Article(Shop shop)
        {
            Shop = shop;
            ArticleDetails = new ArticleDetails();
            Image = new Image();
            Price = new Price();
        }
        public Article(Article article)
        {
            ArticleId = ArticleId;
            ArticleIdentifier = ArticleIdentifier;
            Shop = new Shop(article.Shop);
            ArticleDetails = new ArticleDetails(article.ArticleDetails);
            Image = new Image(article.Image);
            Price = new Price(article.Price);
        }
        public override string ToString()
        {
            return string.Format("{0}{1}{2}{3}{4}", ArticleIdentifier, ArticleDetails, Shop, Image, Price);
        }
    }
}