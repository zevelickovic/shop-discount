﻿namespace ShopDiscount.Models.Client
{
    public class ListViewInfo
    {
        public int Count { get; set; }
        public int Page { get; set; }
        public int Order { get; set; }

        public ListViewInfo(int count, int page, int order)
        {
            Count = count;
            Page = page;
            Order = order;
        }
    }
}