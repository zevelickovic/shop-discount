﻿using System.Runtime.Serialization;

namespace ShopDiscount.Models.Client
{
    [DataContract]
    public class Pagination
    {
        [DataMember]
        public int Count { get; set; }
        [DataMember]
        public int Page { get; set; }
        [DataMember]
        public int Order { get; set; }
        [DataMember]
        public int TotalPages { get; set; }

        public bool IsPrevExist { get { return Page > 1; } }
        public bool IsNextExist { get { return Page < TotalPages; } }

        public Pagination(int count, int page, int order)
        {
            Count = count;
            Page = page;
            Order = order;
        }

        
    }
}