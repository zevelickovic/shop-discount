﻿using System.Runtime.Serialization;

namespace ShopDiscount.Models.Client
{
    [DataContract]
    public class ArticleListModel
    {
        [DataMember]
        public SimpleArticle [] Articles { get; set; }
        [DataMember]
        public Pagination Pagination { get; set; }

        public ArticleListModel(int count, int page, int order)
        {
            Pagination = new Pagination(count, page, order);
        }
    }
}
