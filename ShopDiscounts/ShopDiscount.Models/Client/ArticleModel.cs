﻿namespace ShopDiscount.Models.Client
{
    public class ArticleModel
    {
        public Article Article { get; set; }
        public ListViewInfo ListViewInfo { get; set; }
        public ArticleModel(Article article, int count, int page, int order)
        {
            Article = new Article(article);
            ListViewInfo = new ListViewInfo(count, page, order);
        }
    }
}
