﻿namespace ShopDiscount.Models.Responses
{
    public class ArticleAddOrUpdateResponse
    {
        public long ArticleId { get; private set; }
        public bool IsNew { get; private set; }
        public bool IsValid { get; private set; }
        public ArticleAddOrUpdateResponse(long articleId, bool isNew)
        {
            ArticleId = articleId;
            IsNew = isNew;
            IsValid = true;
        }

        public ArticleAddOrUpdateResponse()
        {
            IsValid = false;
        }
    }
}