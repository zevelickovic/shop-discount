﻿using ShopDiscount.Models;
using ShopDiscount.Models.Client;
using ShopDiscounts.Cash;
using System.Collections.Generic;
using System.ServiceModel;

namespace ShopDiscount.Wcf.Persister.Services
{
    [ServiceContract]
    public interface IPersisterService
    {
        [OperationContract]
        void BulkInsertArticles(IEnumerable<Article> articles);
        [OperationContract]
        Article GetArticleDetals(long articleId);
        [OperationContract]
        ArticleListModel GetArticleListModel(int count, int page, ArticleOrder order);
        [OperationContract]
        IEnumerable<Shop> GetShops();
    }
}
