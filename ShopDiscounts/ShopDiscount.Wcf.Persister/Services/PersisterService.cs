﻿using System;
using System.ServiceModel;
using ShopDiscount.Models;
using System.Collections.Generic;
using ShopDiscounts.Database;
using ShopDiscount.Models.Responses;
using System.Reflection;
using log4net;
using System.Linq;
using ShopDiscounts.Cash;
using ShopDiscount.Models.Client;

namespace ShopDiscount.Wcf.Persister.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Single)]
    public class PersisterService : IPersisterService
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public PersisterService()
        {
            try
            {
                CashHolder.PullFromDatabase();
            }
            catch (Exception e)
            {
                _log.Error(e);
            }
        }
        public void BulkInsertArticles(IEnumerable<Article> articles)
        {
            _log.InfoFormat("[BulkInsertArticles] for {0} elements", articles.Count());

            foreach (var article in articles)
            {
                var db = new ShopDiscountDb("ShopDiscounts");
                ArticleAddOrUpdateResponse response;
                if (db.TryAddOrUpdateArtcile(article, out response) && response.IsValid)
                {
                    article.ArticleId = response.ArticleId;
                    _log.InfoFormat("Accepted article {0}  with id {1}", article.ArticleIdentifier, response.ArticleId);
                    if (response.IsNew)
                        CashHolder.InsertArticle(article);
                    else
                        CashHolder.UpdateArticle(article);
                }
                else
                {
                    _log.WarnFormat("rejected article {0}", article.ArticleIdentifier);
                }
            }
        }

        public Article GetArticleDetals(long articleId)
        {
            try
            {
                var article = CashHolder.GetArticleDetals(articleId);
                return article;
            }
            catch (Exception e)
            {
                _log.Error(e);
                return null;
            }
        }

        public IEnumerable<Shop> GetShops()
        {
            try
            {
                var db = new ShopDiscountDb("ShopDiscounts");
                var shops = db.GetShops();

                return shops.ToArray();
            }
            catch (Exception e)
            {
                _log.Error(e);
                return null;
            }
        }
        public ArticleListModel GetArticleListModel(int count, int page, ArticleOrder order)
        {
            try
            {
                return CashHolder.GetArticleListModel(count, page, order);
            }
            catch (Exception e)
            {
                _log.Error(e);
                return null;
            }
        }
    }
}
