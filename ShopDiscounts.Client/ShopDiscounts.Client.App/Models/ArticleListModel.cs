using System.Collections.Generic;
using ShopDiscount.Models;

namespace ShopDiscounts.Client.App.Models
{
    public class ArticleListModel
    {
        public List<SimpleArticle> Articles { get; set; }
        public ListViewInfo ListViewInfo { get; set; }

        public ArticleListModel(int count, int page, int order)
        {
            Articles = new List<SimpleArticle>();
            ListViewInfo = new ListViewInfo(count, page, order);
        }
    }
}