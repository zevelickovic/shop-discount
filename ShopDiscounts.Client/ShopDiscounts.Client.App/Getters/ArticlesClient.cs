using System;
using ShopDiscount.Models;
using Newtonsoft.Json;
using ShopDiscount.Models.Client;

namespace ShopDiscounts.Client.App.Getters
{
    public class ArticlesClient
    {
        private AppWebClient _client;

        public ArticlesClient()
        {
            _client = new AppWebClient();
        }

        public ArticleListModel GetArticleListModel(int count, int page, int order)
        {
            try
            {

                var url = string.Format("http://5.189.166.103:18560/API/ApiService.svc/rest/list?count={0}&page={1}&order={2}", count, page, order);
                _client.SetRetries(10);
                _client.SetTimeout(7);
                var json = _client.Get(url);
                var articles = JsonConvert.DeserializeObject<ArticleListModel>(json);
                return articles;
            }
            catch(Exception)
            {
                return null;
            }
        }

        public Article GetArticleDetails(int id)
        {
            try
            {
                var url = string.Format("http://5.189.166.103:18560/API/ApiService.svc/rest/details?articleId={0}", id);
                _client.SetRetries(10);
                _client.SetTimeout(7);
                var json = _client.Get(url);
                var article = JsonConvert.DeserializeObject<Article>(json);
                return article;
            }
            catch(Exception)
            {
                return new Article();
            }
        }
    }
}