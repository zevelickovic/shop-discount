using System;
using System.Net;

namespace ShopDiscounts.Client.App.Getters
{
    public class AppWebClient : WebClient
    {
        private int _timeout = 3;
        private int _retries = 3;

        public void SetTimeout(int timeout)
        {
            _timeout = timeout;
        }
        public void SetRetries(int retries)
        {
            _retries = retries;
        }

        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest w = base.GetWebRequest(uri);
            w.Timeout = _timeout * 1000;
            return w;
        }

        public string Get(string url)
        {
            var retries = _retries;
            while (true)
            {
                try
                {
                    return DownloadString(url);
                }
                catch(Exception)
                {
                    if (retries-- <= 0)
                        throw;
                }
            }
        }
    }
}