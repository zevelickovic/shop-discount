﻿using System;
using Android.App;
using Android.Webkit;
using Android.OS;
using ShopDiscounts.Client.App.Views;
using ShopDiscounts.Client.App.Getters;
using System.Collections.Generic;
using ShopDiscount.Models.Client;

namespace ShopDiscounts.Client.App
{
    
    [Activity(Label = "Akcije", MainLauncher = true)]
    public class MainActivity : Activity
    {
        public static int _counter = 0;
        private static ArticlesClient _client = new ArticlesClient();
        private static WebView _webView;
        private static string _action = "list";
        private static string[] _args = { "10", "1", "1"};

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Main);

            _webView = FindViewById<WebView>(Resource.Id.webView);
            _webView.Settings.JavaScriptEnabled = true;
            _webView.SetWebViewClient(new InternalWebViewClient());

            RenderPage(_action, _args);
        }

        #region Internal Methods

        private static ArticleListModel CollectArticleList(int count, int pageNo, int order)
        {
            try
            {
                var articles = new ArticleListModel(count, pageNo, order);
                var articlesList = _client.GetArticleListModel(count, pageNo, order);
                articles.Articles = articlesList.Articles;
                articles.Pagination = articlesList.Pagination;
                return articles;
            }
            catch (Exception)
            {
                return new ArticleListModel(10,1,1);
            }
        }
        private static ArticleModel CollectArticleDetails(int articleId, int count, int page, int order)
        {
            try
            {
                var article = _client.GetArticleDetails(articleId);
                var articleModel = new ArticleModel(article, count, page, order);
                return articleModel;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static void RenderArticleListWebView(int count, int pageNo, int order)
        {
            var articles = CollectArticleList(count, pageNo, order);
            var template = new RazorArticles() { Model = articles };
            var page = template.GenerateString();

            _webView.LoadDataWithBaseURL("file:///android_asset/", page, "text/html", "UTF-8", null);
        }

        private static void RenderArticleDetailsWebView(int articleId, int count, int pageNo, int order)
        {
            var article = CollectArticleDetails(articleId, count, pageNo, order);
            var template = new RazorDetails() { Model = article };
            var page = template.GenerateString();
            page = page.Replace("<div class=\"body-table\"></div>", article.Article.ArticleDetails.HtmlBody);

            _webView.LoadDataWithBaseURL("file:///android_asset/", page, "text/html", "UTF-8", null);
        }

        private static void RenderPage(string action, string[] args)
        {
            _action = action;
            _args = args;
            switch (action)
            {
                case "details":
                    RenderArticleDetailsWebView(int.Parse(args[0]), int.Parse(args[1]), int.Parse(args[2]), int.Parse(args[2]));
                    break;
                case "list":
                    RenderArticleListWebView(int.Parse(args[0]), int.Parse(args[1]), int.Parse(args[2]));
                    break;
                default:
                    RenderArticleListWebView(10, 1, 1);
                        break;
            }
        }

        private class InternalWebViewClient : WebViewClient
        {
            public override bool ShouldOverrideUrlLoading(WebView view, string url)
            {
                if(url.Contains("action:"))
                {
                    var array = url.Split(':');
                    var action = array[1];
                    List<string> argsList = new List<string>();
                    for (int i = 2; i < array.Length; i++)
                        argsList.Add(array[i]);
                    RenderPage(action, argsList.ToArray());
                }
                return true;
            }
        }
        #endregion
    }
}

